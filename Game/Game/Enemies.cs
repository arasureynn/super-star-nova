﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace SuperStarNova
{
    class Enemy
    {
        Texture2D texture;
        Vector2 location;
        Rectangle initialFrame;
        int enemyHealth;
        int enemyDamage;
        int frameCount;
        int speed;
        private bool isBoss;
        float fireTime;
        int XP;

        public Enemy(
            Texture2D texture,
            Vector2 location,
            Rectangle initialFrame,
            int enemyHealth,
            int enemyDamage,
            int frameCount,
            int speed,
            int XP,
            float fireTime,
            bool isBoss
            )
        {
            this.texture = texture;
            this.location = location;
            this.initialFrame = initialFrame;
            this.enemyHealth = enemyHealth;
            this.enemyDamage = enemyDamage;
            this.frameCount = frameCount;
            this.speed = speed;
            this.isBoss = isBoss;
            this.fireTime = fireTime;
            this.XP = XP;
        }

        public Texture2D getTexture()
        {
            return (texture);
        }

        public Vector2 getLocation()
        {
            return (location);
        }

        public Rectangle getInitialFrame()
        {
            return (initialFrame);
        }

        public int getHealth()
        {
            return (enemyHealth);
        }

        public int getDamage()
        {
            return (enemyDamage);
        }

        public int getFrameCount()
        {
            return (frameCount);
        }

        public int getSpeed()
        {
            return (speed);
        }

        public float getShotTimer()
        {
            return (fireTime);
        }

        public int getXP()
        {
            return (XP);
        }

        public bool getIsBoss()
        {
            return (isBoss);
        }
    }

    class Enemy2
    {
        public Sprite EnemySprite;
        private Queue<Vector2> Waypoints = new Queue<Vector2>();
        private Queue<Vector2> BossWaypoints = new Queue<Vector2>();
        private Vector2 currentWaypoint = Vector2.Zero;
        private float speed = 120f;
        public bool Destroyed = false;
        private int enemyRadius = 40;
        private Vector2 previousLocation = Vector2.Zero;
        public int Health = 10;
        public int Damage = 10;
        public TimeSpan fireTime;
        public TimeSpan previousFireTime;
        int XP;
        private bool isBoss = false;
        public bool firstWaypointReached = false;

        public Vector2 Direction
        {
            get { return direction; }
            set { direction = value; }
        }
        protected Vector2 direction;

        const float atDestinationLimit = 5f;

        public float DistanceToDestination
        {
            get { return Vector2.Distance(EnemySprite.Location, Waypoints.Peek()); }
        }

        public bool AtDestination
        {
            get { return DistanceToDestination < atDestinationLimit; }
        }

        public Enemy2(
            Texture2D texture,
            Vector2 location,
            Rectangle initialFrame,
            int enemyHealth,
            int enemyDamage,
            int frameCount,
            int speed,
            int XP,
            float fireTime,
            bool isBoss)
        {
            EnemySprite = new Sprite(
                location,
                texture,
                initialFrame,
                Vector2.Zero);

            for (int x = 1; x < frameCount; x++)
            {
                EnemySprite.AddFrame(
                    new Rectangle(
                    initialFrame.X = (initialFrame.Width * x),
                    initialFrame.Y,
                    initialFrame.Width,
                    initialFrame.Height));
            }

            Health = enemyHealth;
            Damage = enemyDamage;
            previousLocation = location;
            currentWaypoint = location;
            EnemySprite.CollisionRadius = enemyRadius;
            this.speed = speed;
            this.XP = XP;
            this.fireTime = TimeSpan.FromSeconds(fireTime);
            this.isBoss = isBoss;
        }

        public void AddWaypoint(Vector2 waypoint)
        {
            Waypoints.Enqueue(waypoint);
        }

        public bool WaypointReached()
        {
            if (Vector2.Distance(EnemySprite.Location, currentWaypoint) <
                (float)EnemySprite.Source.Width / 2)
            {
                return true;
            }
            return false;
        }

        public bool IsActive()
        {
            if (Destroyed)
                return false;

            if (Health <= 0)
                return false;

            if (Waypoints.Count <= 0)
                return false;

            if (Waypoints.Count > 0)
                return true;



            return true;
        }

        public int getXP()
        {
            return (XP);
        }

        public bool IsBoss()
        {
            return (isBoss);
        }

        public void Update(GameTime gameTime)
        {
            EnemySprite.Update(gameTime);
            if (IsActive())
            {
                float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

                // If we have any waypoints, the first one on the list is where 
                // we want to go
                if (Waypoints.Count > 0)
                {
                    if (AtDestination)
                    {
                        if (IsBoss())
                        {
                            Waypoints.Enqueue(Waypoints.Dequeue());
                        }
                        else
                        {
                            Waypoints.Dequeue();
                        }
                        firstWaypointReached = true;
                    }
                    else
                    {
                        direction = -(EnemySprite.Location - Waypoints.Peek());

                        direction.Normalize();
                        Direction = direction;

                        EnemySprite.Location = EnemySprite.Location + (Direction *
                            speed * elapsedTime);
                    }
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsActive())
            {
                EnemySprite.Draw(spriteBatch);
            }
        }
    }
}