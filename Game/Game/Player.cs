﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SuperStarNova
{
    class Player
    {
        public Vector2 Position;
        public bool Active;
        public int Health;
        public int Experience;
        public Animation PlayerAnimation;

        public int width
        {
            get { return PlayerAnimation.FrameWidth; }
        }

        public int height
        {
            get { return PlayerAnimation.FrameHeight; }
        }

        public void Initialize(Animation animation, Vector2 position)
        {
            PlayerAnimation = animation;
            Position = position;
            Active = true;
            Health = 100;
            Experience = 0;
        }

        public bool isAlive()
        {
            if (Health == 0)
                return false;
            else
                return true;
        }

        public void Update(GameTime gameTime)
        {
            PlayerAnimation.Position = Position;
            PlayerAnimation.Update(gameTime);
        }

        public void Draw(SpriteBatch spritez)
        {
            PlayerAnimation.Draw(spritez);
        }
    }
}
