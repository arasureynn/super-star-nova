using System;

namespace SuperStarNova
{
    class Levels
    {
        private int CurrentLevel = 0;
        private int MinShipsPerWave;
        private int MaxShipsPerWave;
        private float nextWaveMinTimer;
        private int[] TypeToSpawn;
        private int NumberofEnemies;
        private int BossType;
        private bool GameComplete = false;
        private bool HasBoss = false;

        Random rand = new Random();

        public void SelectLevel(int levelnumber)
        {
            CurrentLevel = levelnumber;
            switch (CurrentLevel)
            {
                case 1:
                    MinShipsPerWave = 1;
                    MaxShipsPerWave = 1;
                    nextWaveMinTimer = 1.5f;
                    TypeToSpawn = new int[] { 0, 1 };
                    NumberofEnemies = 4;
                    HasBoss = false;
                    GameComplete = false;
                    break;
                case 2:
                    MinShipsPerWave = 1;
                    MaxShipsPerWave = 2;
                    nextWaveMinTimer = 1.3f;
                    TypeToSpawn = new int[] { 0, 1, 2 };
                    NumberofEnemies = 5;
                    HasBoss = true;
                    BossType = 0;
                    GameComplete = false;
                    break;

                case 3:
                    MinShipsPerWave = 1;
                    MaxShipsPerWave = 2;
                    nextWaveMinTimer = 1.0f;
                    TypeToSpawn = new int[] { 0, 1, 2, 3 };
                    NumberofEnemies = 6;
                    HasBoss = false;
                    GameComplete = false;
                    break;

                case 4:
                    MinShipsPerWave = 1;
                    MaxShipsPerWave = 2;
                    nextWaveMinTimer = 1.0f;
                    TypeToSpawn = new int[] { 1, 2, 3 };
                    NumberofEnemies = 6;
                    HasBoss = false;
                    GameComplete = true;
                    break;

                default:
                    GameComplete = true;
                    break;
            }
        }

        public int GetCurrentLevel()
        {
            return (CurrentLevel);
        }

        public int GetMinShipsPerWave()
        {
            return (MinShipsPerWave);
        }

        public int GetMaxShipsPerWave()
        {
            return (MaxShipsPerWave);
        }

        public int GetEnemySpawnType()
        {
            return (TypeToSpawn[rand.Next(0, TypeToSpawn.Length)]);
        }

        public float GetnextWaveMinTimer()
        {
            return (nextWaveMinTimer);
        }

        public int GetBossType()
        {
            return (BossType);
        }

        public int GetNumberofEnemies()
        {
            return (NumberofEnemies);
        }

        public bool GetGameComplete()
        {
            return (GameComplete);
        }

        public bool GetHasBoss()
        {
            return (HasBoss);
        }
    }

}
