﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.GamerServices;
using SuperStarNova.Inputs;
using SuperStarNova.Managers;

namespace SuperStarNova
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        #region Fields
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Player player;

        private Vector2 initialPlayerPosition;

        Levels levels = new Levels();

        private EnemyManager enemyManager;
        private BackgroundField backgroundField;
        private ParticleManager particleManager;
        private CollisionManager collisionManager;
        private ExplosionManager explosionManager;
        private HighScoreManager highScoreManager;
        private SettingsManager settingsManager;

        private Texture2D Fire1;
        private Texture2D Fire2;
        private Texture2D Fire3;
        private Texture2D WeaponNotAvailable;
        private Texture2D SettingsIcon;

        private GameInput gameInput;
        private TouchIndicatorCollection touchIndicator;
        private Texture2D square;

        Rectangle secretRectangle;

        Rectangle FireRectangle = new Rectangle(730,440,50,50);
        Rectangle Fire2Rectangle = new Rectangle(660,440,50,50);
        Rectangle BombRectangle = new Rectangle(590, 440, 50, 50);
        Rectangle PauseRectangle = new Rectangle(0,440,50,50);

        //blue
        Rectangle UpRectangle = new Rectangle(95, 325, 50, 50);
        //red
        Rectangle DownRectangle = new Rectangle(95, 410, 50, 50);
        //yellow
        Rectangle RightRectangle = new Rectangle(10, 400, 50, 50);
        //green
        Rectangle LeftRectangle = new Rectangle(180, 400, 50, 50);

        float PlayerSpeed;

        //parallaxing layers
        ParallaxingBackground bgLayer1;
        ParallaxingBackground bgLayer2;

        //menus
        Menu mainMenu;
        Menu levelCompleteMenu;
        Menu gameCompleteMenu;
        Menu missionFailedMenu;
        Menu gamePausedMenu;
        Menu settingsMenu;

        // The rate at which the enemies appear
        TimeSpan enemySpawnTime;
        TimeSpan previousSpawnTime;

        // A random nuber generator
        Random rand;

        Texture2D projectileTexture;

        TimeSpan fireTime;
        TimeSpan previousFireTime;
        TimeSpan gameTimeSpan;

        Texture2D explosionTexture;

        SoundEffect playerLaserSound;
        SoundEffect enemyLaserSound;

        Song gameplayMusic;

        int BombCount = 3;
        SpriteFont font;

        private int screenWidth = 800;
        private int screenHeight = 480;

        public int ScreenWidth
        {
            get { return screenWidth; }
            set { screenWidth = value; }
        }

        public int ScreenHeight
        {
            get { return screenHeight; }
            set { screenHeight = value; }
        }

        public enum GameState
        {
            MainMenu,
            Loading,
            GamePlay,
            GamePause,
            GameFail,
            NextLevel,
            HighScores,
            GameComplete,
            SecretDebug,
            Settings
        }

        GameState gameState = GameState.Loading;

        public string playerName;
        public bool firstLaunch = false;
        public bool gameFailedHighScore = false;
        private bool bossMode = false;

        #endregion

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // Frame rate of 30 Frames Per Second
            TargetElapsedTime = TimeSpan.FromTicks(333333);

#if WINDOWS_PHONE
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 480;

            graphics.IsFullScreen = true;
#endif
#if WINDOWS
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 480;
            graphics.IsFullScreen = false;
#endif
#if XBOX
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferredBackBufferWidth = 1280;
#endif
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            //Initialize the player class
            player = new Player();
            
            // Set a constant player move speed
            PlayerSpeed = 8.0f;

            //Enable the FreeDrag gesture.
            TouchPanel.EnabledGestures = GestureType.FreeDrag | GestureType.Tap;

            gameInput = new GameInput();
            touchIndicator = new TouchIndicatorCollection();

            bgLayer1 = new ParallaxingBackground();
            bgLayer2 = new ParallaxingBackground();

            mainMenu = new Menu();
            levelCompleteMenu = new Menu();
            gameCompleteMenu = new Menu();
            missionFailedMenu = new Menu();
            gamePausedMenu = new Menu();
            settingsMenu = new Menu();

            // Set the time keepers to zero
            previousSpawnTime = TimeSpan.Zero;

            // Used to determine how fast enemy respawns
            enemySpawnTime = TimeSpan.FromSeconds(1.0f);

            // Initialize our random number generator
            rand = new Random();

            // Set the laser to fire every quarter second
            fireTime = TimeSpan.FromSeconds(.25f);
            
            AddInputs();

            AddMenu();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            settingsManager = new SettingsManager(1);
            highScoreManager = new HighScoreManager(10);
            CheckForFiles();

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Load the player resources
            Animation playerAnimation = new Animation();
            Texture2D playerTexture = Content.Load<Texture2D>("player");

            playerAnimation.Initialize(
                playerTexture, 
                Vector2.Zero, 
                75, 52, 7, 60, 
                Color.White, 1f, 
                true);

            Fire1 = Content.Load<Texture2D>("UI/weaponbutton1");
            Fire2 = Content.Load<Texture2D>("UI/weaponbutton2");
            Fire3 = Content.Load<Texture2D>("UI/weaponbutton3");
            WeaponNotAvailable = Content.Load<Texture2D>("UI/weaponnotavailable");
            SettingsIcon = Content.Load<Texture2D>("UI/settings");
            secretRectangle = new Rectangle(0,0,SettingsIcon.Width/2,SettingsIcon.Height/2);

            initialPlayerPosition = new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X, 
                GraphicsDevice.Viewport.TitleSafeArea.Y
                + GraphicsDevice.Viewport.TitleSafeArea.Height / 2);

            // Load the parallaxing background
            bgLayer1.Initialize(Content, "bgLayer1-1", GraphicsDevice.Viewport.Width, -1);
            bgLayer2.Initialize(Content, "bgLayer1-2", GraphicsDevice.Viewport.Width, -2);

            projectileTexture = Content.Load<Texture2D>("laser");
            
            explosionTexture = Content.Load<Texture2D>("explosion2");
            explosionManager = new ExplosionManager();

            explosionManager.Initialize(explosionTexture, 134, 134, 12, 45, Color.White, 1f, false);

            // Load the music
            gameplayMusic = Content.Load<Song>("sound/midnight-ride_sj");

            // Load the laser and explosion sound effect
            playerLaserSound = Content.Load<SoundEffect>("sound/laser01_sjedit");
            enemyLaserSound = Content.Load<SoundEffect>("sound/laser02_sj");

            // Load the score font
            font = Content.Load<SpriteFont>("Fonts/nokiaTestFont");
            square = Content.Load<Texture2D>("UI/Pixel");
            //AddInputs();

            // Start the music right away
            PlayMusic(gameplayMusic);

            font = Content.Load<SpriteFont>("Fonts/gameFont");

            Texture2D spriteSheet = Content.Load<Texture2D>("Enemies/enemySpriteSheet");
            Texture2D tinySprite = Content.Load<Texture2D>("tinySprite");

            backgroundField = new BackgroundField(
                screenWidth,screenHeight,
                300,new Vector2(0,30f),
                tinySprite,
                new Rectangle(0,0,2,2));

            enemyManager = new EnemyManager(
                spriteSheet,
                new Rectangle(
                    0,0,
                    GraphicsDevice.Viewport.TitleSafeArea.Width,
                    GraphicsDevice.Viewport.TitleSafeArea.Height),
                    levels,
                    projectileTexture,
                    enemyLaserSound);

            Texture2D particlesSprite = Content.Load<Texture2D>("laser");

            particleManager = new ParticleManager(
                particlesSprite, 500, Constants.MaxParticleCount, 0f, 2.5f, 
                .1f, .2f, 0f, 300f, 0f, 10f, 5f, 10f);

            particleManager.AddAlphaColor(Color.Red);
            particleManager.AddAlphaColor(Color.Green);
            particleManager.AddAlphaColor(Color.Blue);
            
            collisionManager = new CollisionManager(
                player, enemyManager, particleManager, 
                explosionManager, GraphicsDevice);

            player.Initialize(playerAnimation,
                initialPlayerPosition,
                projectileTexture,
                new Rectangle(0, 0, 
                    GraphicsDevice.Viewport.TitleSafeArea.Width, 
                    GraphicsDevice.Viewport.TitleSafeArea.Height), 
                    playerLaserSound);
        }

        private void CheckForFiles()
        {
            if (!settingsManager.CheckForSettingsFile(Constants.SettingsManagerSaveFile))
            {
                System.Diagnostics.Debug.WriteLine("Loading defaults...");
                settingsManager.LoadDefaultSettings();
                playerName = Constants.DefaultPlayerName;
                firstLaunch = true;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("loading from settings");
                SettingsManager data = settingsManager.LoadSettings(Constants.SettingsManagerSaveFile);
                playerName = data.PlayerName[0].ToString() + " (Loaded from settings)";
            }

            if (!highScoreManager.CheckForSavedHighScores(Constants.HighScoreSaveFile))
            {
                System.Diagnostics.Debug.WriteLine("generating default high scores...");
                highScoreManager.Initialize();
            }
        }

        private void PlayMusic(Song song)
        {
            try
            {
                MediaPlayer.Play(song);
                MediaPlayer.IsRepeating = true;
            }
            catch { }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        private void AddInputs()
        {
            // add inputs for secret rectangle
            gameInput.AddTouchTapInput(Constants.Secret,secretRectangle,false);

            // add inputs for fire1
            gameInput.AddGamePadInput(Constants.Fire1,Buttons.A,true);
            gameInput.AddKeyboardInput(Constants.Fire1,Keys.LeftControl,false);
            gameInput.AddTouchTapInput(Constants.Fire1, FireRectangle, false);

            // Add inputs for fire2
            gameInput.AddGamePadInput(Constants.Fire2, Buttons.X, true);
            gameInput.AddKeyboardInput(Constants.Fire2, Keys.LeftShift, true);
            gameInput.AddTouchTapInput(Constants.Fire2, Fire2Rectangle, true);

            // add inputs for bomb
            gameInput.AddGamePadInput(Constants.Bomb, Buttons.Y, true);
            gameInput.AddKeyboardInput(Constants.Bomb, Keys.Space, true);
            gameInput.AddTouchTapInput(Constants.Bomb, BombRectangle, true);

            // add inputs for pause
            gameInput.AddGamePadInput(Constants.Pause,Buttons.Start,true);
            gameInput.AddKeyboardInput(Constants.Pause,Keys.P,true);
            gameInput.AddTouchTapInput(Constants.Pause,PauseRectangle,true);

            // add inputs for up
            gameInput.AddGamePadInput(Constants.Up, Buttons.LeftThumbstickUp, true);
            gameInput.AddKeyboardInput(Constants.Up, Keys.W, true);
            gameInput.AddKeyboardInput(Constants.Up, Keys.Up, true);
            gameInput.AddTouchTapInput(Constants.Up, UpRectangle, true);

            // add inputs for down
            gameInput.AddGamePadInput(Constants.Down, Buttons.LeftThumbstickDown, true);
            gameInput.AddKeyboardInput(Constants.Down, Keys.Down, true);
            gameInput.AddKeyboardInput(Constants.Down, Keys.S, true);
            gameInput.AddTouchTapInput(Constants.Down, DownRectangle, true);

            // add inputs for right
            gameInput.AddGamePadInput(Constants.Right, Buttons.LeftThumbstickRight, true);
            gameInput.AddKeyboardInput(Constants.Right, Keys.A, true);
            gameInput.AddKeyboardInput(Constants.Right, Keys.Left, true);
            gameInput.AddTouchTapInput(Constants.Right, RightRectangle, true);
            
            // add inputs for left
            gameInput.AddGamePadInput(Constants.Left, Buttons.LeftThumbstickLeft, true);
            gameInput.AddKeyboardInput(Constants.Left, Keys.D, true);
            gameInput.AddKeyboardInput(Constants.Left, Keys.Right, true);
            gameInput.AddTouchTapInput(Constants.Left, LeftRectangle, true);
        }

        private void AddMenu()
        {
            mainMenu.Initialize(Content, 
                "bgLayer1-1", 
                GraphicsDevice.Viewport.Width,
                GraphicsDevice.Viewport.Height, -1,
                "UI/smallmenubutton", 
                "UI/smallmenubuttonselected", 
                "Super Star Nova"
                //,Keys.Up, Keys.Down, Keys.Enter
                );
            mainMenu.AddItem("Start New Game", "NewGame");
            mainMenu.AddItem("Resume Game", "ResumeGame");
            mainMenu.AddItem("High Scores", "HighScore");
            mainMenu.AddItem("Particle Test","DEBUG");

            levelCompleteMenu.Initialize(Content, 
                "bgLayer1-2", 
                GraphicsDevice.Viewport.Width,
                GraphicsDevice.Viewport.Height, -1, 
                "UI/smallmenubutton", 
                "UI/smallmenubuttonselected", 
                "Level Complete"
                //,Keys.Up, Keys.Down, Keys.Enter
                );
            levelCompleteMenu.AddItem("Continue Mission", "ContinueMission");
            levelCompleteMenu.AddItem("Save", "Save");
            levelCompleteMenu.AddItem("Main Menu", "MainMenu");

            gameCompleteMenu.Initialize(Content,
                "bgLayer1-2",
                GraphicsDevice.Viewport.Width,
                GraphicsDevice.Viewport.Height, -1,
                "UI/smallmenubutton",
                "UI/smallmenubuttonselected",
                "Game Complete"
                //,Keys.Up, Keys.Down, Keys.Enter
                );
            gameCompleteMenu.AddItem("Main Menu", "MainMenu");
            gameCompleteMenu.AddItem("High Scores", "HighScore");

            missionFailedMenu.Initialize(Content, 
                "bgLayer1-2", 
                GraphicsDevice.Viewport.Width,
                GraphicsDevice.Viewport.Height, -1, 
                "UI/smallmenubutton", 
                "UI/smallmenubuttonselected", 
                "Mission Failed"
                //,Keys.Up, Keys.Down, Keys.Enter
                );
            missionFailedMenu.AddItem("Restart Level", "RestartLevel");
            missionFailedMenu.AddItem("Main Menu", "MainMenu");

            gamePausedMenu.Initialize(Content,
                "bgLayer1-2",
                GraphicsDevice.Viewport.Width,
                GraphicsDevice.Viewport.Height, -1,
                "UI/smallmenubutton",
                "UI/smallmenubuttonselected",
                "Game Paused"
                //,Keys.Up, Keys.Down, Keys.Enter
                );
            gamePausedMenu.AddItem("Resume Mission", "ResumeMission");
            gamePausedMenu.AddItem("Save", "Save");
            gamePausedMenu.AddItem("Main Menu", "MainMenu");

            settingsMenu.Initialize(Content,
                "bgLayer1-2",
                GraphicsDevice.Viewport.TitleSafeArea.Width,
                GraphicsDevice.Viewport.TitleSafeArea.Height,
                -1,"UI/smallMenuButton",
                "UI/smallmenubuttonselected","Game Settings");
            settingsMenu.AddItem("Delete Settings","DeleteSettings");
            settingsMenu.AddItem("Delete High Scores","DeleteScores");
        }

        private void StartNewGame(GameTime gameTime)
        {
            player.PlayerScore = 0;
            player.Health = 100;
            player.Experience = 0;

            StartLevel(gameTime, 1);
        }

        private void StartLevel(GameTime gameTime, int level)
        {
            ResetPlayScreen();
            levels.SelectLevel(level);
            enemyManager.enemiesKilledLevel = 0;
            enemyManager.BossMode = false;
            bossMode = false;

            //load enemy assets for this level
            enemyManager.nextWaveTimer += 5f;
            gameState = GameState.GamePlay;
        }

        private void StartDebugLevel(GameTime gameTime)
        {
            ResetPlayScreen();
            gameState = GameState.SecretDebug;
        }

        private void RandomParticleExplosions(GameTime gameTime)
        {
            TimeSpan explosionTime = TimeSpan.FromSeconds(5f);
            TimeSpan previousExplodeyTime = TimeSpan.Zero;

            Vector2 explodeyLocation;
            Random rand = new Random(DateTime.Now.Millisecond);

            if (gameTime.TotalGameTime - previousExplodeyTime > explosionTime)
            {
                previousExplodeyTime = gameTime.TotalGameTime;
                explodeyLocation = new Vector2(rand.Next(
                    GraphicsDevice.Viewport.TitleSafeArea.Width),
                    rand.Next(GraphicsDevice.Viewport.TitleSafeArea.Height));
                particleManager.AddParticles(explodeyLocation,750);
            }
        }

        private void EndLevel()
        {
            if (levels.GetGameComplete())
            {
                highScoreManager.SaveHighScores("Player1", player.PlayerScore, levels.GetCurrentLevel());
                gameCompleteMenu.StartMenu();
                gameState = GameState.GameComplete;
            }
            else
            {
                levelCompleteMenu.StartMenu();
                gameState = GameState.NextLevel;
            }
        }

        private void UpdatePlayer(GameTime gameTime)
        {
            player.Update(gameTime);

            // Make sure that the player does not go out of bounds
            player.Position.X = MathHelper.Clamp(player.Position.X, player.Width / 2,
                GraphicsDevice.Viewport.Width - player.Width / 2);
            player.Position.Y = MathHelper.Clamp(player.Position.Y, player.Height / 2,
                GraphicsDevice.Viewport.Height - player.Height / 2 - 70f);

            // reset score if player health goes to zero
            if (player.Health <= 0)
            {
                explosionManager.Add(player.Position);
                missionFailedMenu.StartMenu();
                gameFailedHighScore = true;
                gameState = GameState.GameFail;
            }

        }

        private void ResetPlayScreen()
        {
            //remove all enemies
            enemyManager.Enemies.Clear();

            //remove all explosions
            explosionManager.Clear();

            //remove all particles
            particleManager.particles.Clear();

            //remove enemy projectiles
            enemyManager.EnemyShotManager.Shots.Clear();

            //remove player projectiles
            player.PlayerShotManager.Shots.Clear();

            player.Position = initialPlayerPosition;
        }

        private void UpdateXp()
        {
            if (player.Experience < 1000)
            {
                fireTime = TimeSpan.FromSeconds(.24f);
            }
            else if (player.Experience >= 1000)
            {
                fireTime = TimeSpan.FromSeconds(.14f);
            }
            else if (player.Experience >= 1500)
            {
                fireTime = TimeSpan.FromSeconds(.13f);
            }
            else if (player.Experience >= 2000)
            {
                fireTime = TimeSpan.FromSeconds(.12f);
            }
            else if (player.Experience >= 2500)
            {
                fireTime = TimeSpan.FromSeconds(.11f);
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            switch (gameState)
            {
                case GameState.Loading:
                    if (firstLaunch == true)
                    {
#if WINDOWS_PHONE
                        if (!Guide.IsVisible)
                        {
                            Guide.BeginShowKeyboardInput(
                                PlayerIndex.One, "Enter Player Name:", 
                                "Player Name", Constants.DefaultPlayerName,
                                delegate(IAsyncResult result)
                                {
                                    playerName = Guide.EndShowKeyboardInput(result);
                                    settingsManager.SaveSetting(playerName);
                                }, 
                                null);
                            firstLaunch = false;
                        }
#endif
                    }

                    gameState = GameState.MainMenu;
                    break;

                case GameState.MainMenu:
                    if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed
                        || Keyboard.GetState().IsKeyDown(Keys.Escape))
                    {
                        this.Exit();
                    }
                    backgroundField.Update(gameTime);
                    mainMenu.Update(gameTime);

                    if(mainMenu.IsPressed("Quit", PlayerIndex.One, gameTime))
                    {
                        this.Exit();
                    }
                    else if (mainMenu.IsPressed("NewGame", PlayerIndex.One, gameTime))
                    {
                        StartNewGame(gameTime);
                    }
                    else if (mainMenu.IsPressed("HighScore",PlayerIndex.One,gameTime))
                    {
                        gameState = GameState.HighScores;
                    }
                    else if (mainMenu.IsPressed("DEBUG",PlayerIndex.One,gameTime))
                    {
                        StartDebugLevel(gameTime);
                    }
                    break;

                case GameState.NextLevel:
                    if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed
                        || Keyboard.GetState().IsKeyDown(Keys.Escape))
                    {
                        gameState = GameState.MainMenu;
                    }
                    backgroundField.Update(gameTime);
                    particleManager.Update(gameTime);
                    explosionManager.Update(gameTime);
                    UpdatePlayer(gameTime);
                    levelCompleteMenu.Update(gameTime);

                    if (levelCompleteMenu.IsPressed("ContinueMission", PlayerIndex.One, gameTime))
                    {
                        StartLevel(gameTime, levels.GetCurrentLevel() + 1);
                    }
                    else if (levelCompleteMenu.IsPressed("MainMenu",PlayerIndex.One,gameTime))
                    {
                        ResetPlayScreen();
                        gameState = GameState.MainMenu;
                    }
                    break;

                case GameState.GameFail:
                    if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed
                        || Keyboard.GetState().IsKeyDown(Keys.Escape))
                    {
                        gameState = GameState.MainMenu;
                    }
                    if (gameFailedHighScore)
                    {
                        highScoreManager.SaveHighScores(playerName, player.PlayerScore, levels.GetCurrentLevel());
                        gameFailedHighScore = false;
                    }
                    backgroundField.Update(gameTime);
                    particleManager.Update(gameTime);
                    explosionManager.Update(gameTime);
                    enemyManager.Update(gameTime);
                    missionFailedMenu.Update(gameTime);

                    if (missionFailedMenu.IsPressed("RestartLevel", PlayerIndex.One, gameTime))
                    {
                        player.Health = 100;
                        player.PlayerScore = player.PlayerScore / 2;
                        StartLevel(gameTime, levels.GetCurrentLevel());
                    }
                    else if (missionFailedMenu.IsPressed("MainMenu", PlayerIndex.One, gameTime))
                    {
                        gameState = GameState.MainMenu;
                    }
                    break;

                case GameState.GamePause:
                    backgroundField.Update(gameTime);
                    gamePausedMenu.Update(gameTime);
                    if (gamePausedMenu.IsPressed("ResumeMission",PlayerIndex.One,gameTime))
                    {
                        gameState = GameState.GamePlay;
                    } 
                    else if (gamePausedMenu.IsPressed("MainMenu",PlayerIndex.One,gameTime))
                    {
                        ResetPlayScreen();
                        gameState = GameState.MainMenu;
                    }
                    break;

                case GameState.HighScores:

                    if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed
                        || Keyboard.GetState().IsKeyDown(Keys.Escape))
                    {
                        gameState = GameState.MainMenu;
                    }
                    RandomParticleExplosions(gameTime);
                    collisionManager.CheckCollisions();
                    backgroundField.Update(gameTime);
                    particleManager.Update(gameTime);

                    break;

                case GameState.GameComplete:

                    backgroundField.Update(gameTime);
                    particleManager.Update(gameTime);
                    explosionManager.Update(gameTime);
                    gameCompleteMenu.Update(gameTime);

                    if (gameCompleteMenu.IsPressed("HighScore",PlayerIndex.One,gameTime))
                    {
                        gameState = GameState.HighScores;
                    }
                    else if (gameCompleteMenu.IsPressed("MainMenu", PlayerIndex.One, gameTime))
                    {
                        ResetPlayScreen();
                        gameState = GameState.MainMenu;
                    }
                    break;

                case GameState.GamePlay:
                    if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed
                        || Keyboard.GetState().IsKeyDown(Keys.Escape))
                    {
                        particleManager.particles.Clear();
                        gameState = GameState.GamePause;
                    }
                    #region gameplayupdate
                    
                    if ( enemyManager.enemiesKilledLevel >= levels.GetNumberofEnemies())
                    {
                        if (levels.GetHasBoss())
                        {
                            if (!bossMode)
                            {
                                enemyManager.BossMode = true;
                                bossMode = true;
                            }
                            else
                            {
                                if (enemyManager.BossMode == false)
                                {
                                    bossMode = false;
                                    EndLevel();
                                }
                            }

                        }
                        else
                        {
                            EndLevel();
                        }
                    }

                    gameInput.BeginUpdate();

                    if (gameInput.IsPressed(Constants.Fire1))
                    {
                        if (gameTime.TotalGameTime - previousFireTime > fireTime)
                        {
                            previousFireTime = gameTime.TotalGameTime;
                            /*
                            // Add the projectile, but add it to the front and center of the player
                            AddProjectile(player.Position + new Vector2(player.Width, -12), 90.0f, 1);
                            AddProjectile(player.Position + new Vector2(player.Width, 0), 90.0f, 2);
                            AddProjectile(player.Position + new Vector2(player.Width, 12), 90.0f, 1);

                            // Play the laser sound
                            laserSound.Play();
                             */
                            player.FireShot(0, 10);
                        }
                    }

                    if (gameInput.IsPressed(Constants.Fire2))
                    {
                        if (gameTime.TotalGameTime - previousFireTime > fireTime)
                        {
                            previousFireTime = gameTime.TotalGameTime;
                            /*
                            // Add the projectile, but add it to the front and center of the player
                            AddProjectile(player.Position + new Vector2(player.Width, 0), 80.0f, 1);
                            AddProjectile(player.Position + new Vector2(player.Width, 0), 90.0f, 1);
                            AddProjectile(player.Position + new Vector2(player.Width, 0), 100.0f, 1);

                            // Play the laser sound
                            laserSound.Play();
                             */
                            player.FireShot(10, 10);
                        }
                    }

                    if (gameInput.IsPressed(Constants.Up))
                    {
                        player.Position.Y -= PlayerSpeed;
                    }

                    if (gameInput.IsPressed(Constants.Down))
                    {
                        player.Position.Y += PlayerSpeed;
                    }

                    if (gameInput.IsPressed(Constants.Left))
                    {
                        player.Position.X += PlayerSpeed;
                    }

                    if (gameInput.IsPressed(Constants.Right))
                    {
                        player.Position.X -= PlayerSpeed;
                    }

                    backgroundField.Update(gameTime);
                    UpdatePlayer(gameTime);

                    enemyManager.Update(gameTime);

                    gameTimeSpan = gameTime.TotalGameTime;
                    collisionManager.CheckCollisions();
                    particleManager.Update(gameTime);
                    explosionManager.Update(gameTime);
                    UpdateXp();
                    #endregion
                     
                    break;

                case GameState.SecretDebug:

                    if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed
                        || Keyboard.GetState().IsKeyDown(Keys.Escape))
                    {
                        particleManager.particles.Clear();
                        gameState = GameState.MainMenu;
                    }

                    RandomParticleExplosions(gameTime);
                    collisionManager.CheckCollisions();
                    backgroundField.Update(gameTime);
                    explosionManager.Update(gameTime);
                    particleManager.Update(gameTime);
                    break;
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // Start drawing
            spriteBatch.Begin();
            GraphicsDevice.Clear(Color.Black);
            
            switch(gameState)
            {
                case GameState.MainMenu:
                    bgLayer1.Draw(spriteBatch);
                    bgLayer2.Draw(spriteBatch);
                    mainMenu.Draw(spriteBatch);

                    spriteBatch.Draw(SettingsIcon, 
                        new Vector2(
                            GraphicsDevice.Viewport.Width - (SettingsIcon.Width/2),
                            GraphicsDevice.Viewport.Height - (SettingsIcon.Height/2)), 
                            null,Color.White,0f,Vector2.Zero,.5f,SpriteEffects.None,0f);

                    spriteBatch.DrawString(
                        font,
                        playerName,
                        new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X,
                            GraphicsDevice.Viewport.TitleSafeArea.Y),
                        Color.White);

                    break;

                case GameState.NextLevel:
                    bgLayer1.Draw(spriteBatch);
                    bgLayer2.Draw(spriteBatch);

                    particleManager.Draw(spriteBatch);
                    player.Draw(spriteBatch);
                    explosionManager.Draw(spriteBatch);

                    levelCompleteMenu.Draw(spriteBatch);

                    break;

                case GameState.GameFail:
                    bgLayer1.Draw(spriteBatch);
                    bgLayer2.Draw(spriteBatch);

                    enemyManager.Draw(spriteBatch);
                    particleManager.Draw(spriteBatch);
                    explosionManager.Draw(spriteBatch);

                    missionFailedMenu.Draw(spriteBatch);

                    break;

                case GameState.GamePause:

                    bgLayer1.Draw(spriteBatch);
                    bgLayer2.Draw(spriteBatch);
                    gamePausedMenu.Draw(spriteBatch);
                    player.Draw(spriteBatch);
                    enemyManager.Draw(spriteBatch);
                    particleManager.Draw(spriteBatch);
                    explosionManager.Draw(spriteBatch);

                    break;

                case GameState.GameComplete:
                    bgLayer1.Draw(spriteBatch);
                    bgLayer2.Draw(spriteBatch);

                    particleManager.Draw(spriteBatch);
                    player.Draw(spriteBatch);

                    gameCompleteMenu.Draw(spriteBatch);
                    break;

                case GameState.GamePlay:
                    #region gameplaydraw


                    // Draw the moving background
                    backgroundField.Draw(spriteBatch);
                    bgLayer1.Draw(spriteBatch);
                    bgLayer2.Draw(spriteBatch);
                    // Draw the Player
                    player.Draw(spriteBatch);

                    explosionManager.Draw(spriteBatch);
#if WINDOWS_PHONE
                    //Draw Fire Buttons
                    spriteBatch.Draw(Fire1, new Vector2(GraphicsDevice.Viewport.Width - 70f, 
                        GraphicsDevice.Viewport.Height - 40f), Color.White);

                    spriteBatch.Draw(square, FireRectangle, Color.Transparent);
                    spriteBatch.Draw(square, Fire2Rectangle, Color.Transparent);
                    spriteBatch.Draw(square, BombRectangle, Color.Transparent);

                    spriteBatch.Draw(square, UpRectangle, Color.Blue);
                    spriteBatch.Draw(square, DownRectangle, Color.Red);
                    spriteBatch.Draw(square, RightRectangle, Color.Yellow);
                    spriteBatch.Draw(square, LeftRectangle, Color.Green);
            
                    if (player.Experience >= 1000)
                    {
                        spriteBatch.Draw(Fire2, new Vector2(GraphicsDevice.Viewport.Width - 140f, 
                            GraphicsDevice.Viewport.Height - 40f), Color.White);
                    }
                    else
                    {
                        spriteBatch.Draw(WeaponNotAvailable, new Vector2(GraphicsDevice.Viewport.Width - 140f, 
                            GraphicsDevice.Viewport.Height - 40f), Color.White);
                    }

                    if (BombCount > 0)
                    {
                        spriteBatch.Draw(Fire3, new Vector2(GraphicsDevice.Viewport.Width - 210f,
                            GraphicsDevice.Viewport.Height - 40f), Color.White);
                    }
                    else
                    {
                        spriteBatch.Draw(WeaponNotAvailable, new Vector2(GraphicsDevice.Viewport.Width - 210f, 
                            GraphicsDevice.Viewport.Height - 40f), Color.White);
                    }
#endif

                    enemyManager.Draw(spriteBatch);

                    particleManager.Draw(spriteBatch);

                    // Draw the score
                    spriteBatch.DrawString(font, "Score: " + player.PlayerScore,
                                           new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X,
                                                       GraphicsDevice.Viewport.TitleSafeArea.Y),
                                                       Color.White);

                    // Draw the player health
                    spriteBatch.DrawString(font, "Health: " + player.Health,
                                           new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X,
                                                       GraphicsDevice.Viewport.TitleSafeArea.Y + 25),
                                                       Color.White);

                    // Draw the current time
                    spriteBatch.DrawString(font, "Bomb Count: " + BombCount,
                                           new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X,
                                               GraphicsDevice.Viewport.TitleSafeArea.Y + 50),
                                               Color.White);

                    // Draw the current XP
                    spriteBatch.DrawString(font, "Experience: " + player.Experience,
                                           new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X,
                                                       GraphicsDevice.Viewport.TitleSafeArea.Y + 75), 
                                                       Color.White);

                    // Draw the current level
                    spriteBatch.DrawString(font, "Level: " + levels.GetCurrentLevel(),
                                           new Vector2(GraphicsDevice.Viewport.
                                                           TitleSafeArea.X,
                                                       GraphicsDevice.Viewport.
                                                           TitleSafeArea.Y + 100),
                                           Color.White);

                    spriteBatch.DrawString(font, "Enemies to go: " + Math.Max((levels.GetNumberofEnemies() - enemyManager.enemiesKilledLevel), 0).ToString(),
                                           new Vector2(
                                               GraphicsDevice.
                                                   Viewport.
                                                   TitleSafeArea.Width - 300,
                                               GraphicsDevice.
                                                   Viewport.
                                                   TitleSafeArea.Y),
                                           Color.White);

                    #endregion);)
                    break;

                case GameState.HighScores:

                    backgroundField.Draw(spriteBatch);
                    explosionManager.Draw(spriteBatch);
                    particleManager.Draw(spriteBatch);

                    PopulateHighScores();

                    break;
                case GameState.SecretDebug:
                    bgLayer1.Draw(spriteBatch);
                    bgLayer2.Draw(spriteBatch);

                    backgroundField.Draw(spriteBatch);
                    explosionManager.Draw(spriteBatch);
                    particleManager.Draw(spriteBatch);

                    spriteBatch.DrawString(font, "Particles: " + particleManager.particles.Count,
                                           new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X,
                                               GraphicsDevice.Viewport.TitleSafeArea.Y),
                                               Color.White);
                    spriteBatch.DrawString(font, "Max Particles: " + Constants.MaxParticleCount,
                                           new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X,
                                               GraphicsDevice.Viewport.TitleSafeArea.Y + 50),
                                               Color.White);

                    break;
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }

        private void PopulateHighScores()
        {
            HighScoreManager data = highScoreManager.LoadHighScores(Constants.HighScoreSaveFile);

            int xCoord = (GraphicsDevice.Viewport.TitleSafeArea.X+150);
            int yCoord = GraphicsDevice.Viewport.TitleSafeArea.Y + 25;

            for (int i = 0; i < data.Count; i++)
            {
                string highScore = "Player:    " + data.PlayerName[i] + "   " +
                                   "  Score:    " + data.Score[i] + "   " +
                                   "  Levels Complete:    " + data.Level[i];
                spriteBatch.DrawString(font, highScore,
                    new Vector2(xCoord,yCoord),
                        Color.White);
                yCoord += 25;
            }
        }
    }
}