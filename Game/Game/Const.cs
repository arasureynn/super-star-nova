namespace SuperStarNova
{
    class Constants
    {
        public const string Fire1 = "Fire1";
        public const string Fire2 = "Fire2";
        public const string Bomb = "Bomb";
        public const string Pause = "Pause";
        public const string Exit = "Exit";
        public const string Secret = "Secret";

        public const string Up = "Up";
        public const string Down = "Down";
        public const string Right = "Right";
        public const string Left = "Left";

        public const string HighScoreSaveFile = "highscores.dat";
        public const string SettingsManagerSaveFile = "settings.dat";
        public const string DefaultPlayerName = "Player 1";
        public const string GameTitle = "Super Star Nova";

#if WINDOWS
        public const int MaxParticleCount = 20000;
#endif
#if XBOX
        public const int MaxParticleCount = 9000;
#endif
#if WINDOWS_PHONE
        public const int MaxParticleCount = 4000;
#endif
    }
}
