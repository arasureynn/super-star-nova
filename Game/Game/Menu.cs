using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SuperStarNova.Inputs;

namespace SuperStarNova
{
    public class Menu
    {
        Texture2D bgtexture;
        Vector2[] bgpositions;

        int speed;

        SpriteFont font;

        string MenuTitle;

        Texture2D ButtonUp;
        Texture2D ButtonDown;
        
        List<string> MenuItemText = new List<string>();
        List<string> MenuItemAction = new List<string>();
        List<Vector2> MenuItemLocation = new List<Vector2>();
        
        int itemheight = 90;

        int ScreenWidth;
        int ScreenHeight;

        private int SelectedButton = 0;

        private GameInput gameInput;

        private const string MoveSelectedDownAction = "MoveSelectedDown";
        private const string MoveSelectedUpAction = "MoveSelectedUp";
        private const string SelectButtonAction = "SelectButton";

        private static TimeSpan previousMenuMoveTime;
        private TimeSpan menuMoveTime = TimeSpan.FromSeconds(.2f);

        private float FadeTimer = 0f;
        private float FadeForTime = 2f;

        public void Initialize(ContentManager content,
            String texturePath,
            int screenWidth,
            int screenHeight,
            int mSpeed,
            String textureButtonUp,
            String textureButtonDown,
            string menuTitle
            //,Keys selectUp,
            //Keys selectDown,
            //Keys selectKey
            )
        {
            gameInput = new GameInput();

            font = content.Load<SpriteFont>("Fonts/menuFont");

            ScreenWidth = screenWidth;
            ScreenHeight = screenHeight;

            bgtexture = content.Load<Texture2D>(texturePath);
            this.speed = mSpeed;

            bgpositions = new Vector2[ScreenWidth / bgtexture.Width + 1];

            for (int i = 0; i < bgpositions.Length; i++)
            {
                bgpositions[i] = new Vector2(i * bgtexture.Width, 0);
            }

            ButtonUp = content.Load<Texture2D>(textureButtonUp);
            ButtonDown = content.Load<Texture2D>(textureButtonDown);
            MenuTitle = menuTitle;
            AddMenuInputs();
        }

        private void AddMenuInputs()
        {
            gameInput.AddKeyboardInput(MoveSelectedUpAction, Keys.Up, true);
            gameInput.AddKeyboardInput(MoveSelectedDownAction, Keys.Down, true);
            gameInput.AddKeyboardInput(SelectButtonAction, Keys.Enter, true);

            gameInput.AddGamePadInput(MoveSelectedUpAction, Buttons.DPadUp,true);
            gameInput.AddGamePadInput(MoveSelectedDownAction,Buttons.DPadDown,true);
            gameInput.AddGamePadInput(MoveSelectedUpAction, Buttons.LeftThumbstickUp, true);
            gameInput.AddGamePadInput(MoveSelectedDownAction, Buttons.LeftThumbstickDown, true);
            gameInput.AddGamePadInput(SelectButtonAction, Buttons.A, true);
            gameInput.AddGamePadInput(SelectButtonAction, Buttons.Start, true);
        }

        public void AddItem(string text, string action)
        {
            int screenCenter = ScreenWidth / 2;

            Vector2 location = new Vector2(screenCenter - (ButtonUp.Width / 2), itemheight);

            MenuItemText.Add(text);
            MenuItemAction.Add(action);
            MenuItemLocation.Add(location);

            gameInput.AddTouchTapInput(action, 
                new Rectangle((int)location.X, (int)location.Y, 
                    ButtonUp.Width, ButtonUp.Height), 
                false);

            itemheight += 90;
        }

        public void StartMenu()
        {
            FadeTimer = 0f;
        }

        public void Update(GameTime gameTime)
        {
            FadeTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;

            for (int i = 0; i < bgpositions.Length; i++)
            {
                bgpositions[i].X += speed;
                if (speed <= 0)
                {
                    if (bgpositions[i].X <= -bgtexture.Width)
                    {
                        bgpositions[i].X = bgtexture.Width * (bgpositions.Length - 1);
                    }
                }
                else
                {
                    if (bgpositions[i].X >= bgtexture.Width * (bgpositions.Length - 1))
                    {
                        bgpositions[i].X = -bgtexture.Width;
                    }
                }
            }

            if (gameInput.IsPressed(MoveSelectedDownAction) 
                && gameTime.TotalGameTime - previousMenuMoveTime > menuMoveTime)
            {
                previousMenuMoveTime = gameTime.TotalGameTime;
                SelectedButton = (int)MathHelper.Clamp(SelectedButton + 1, 0, MenuItemText.Count - 1);
            }
            else if (gameInput.IsPressed(MoveSelectedUpAction)
                && gameTime.TotalGameTime - previousMenuMoveTime > menuMoveTime)
            {
                previousMenuMoveTime = gameTime.TotalGameTime;
                SelectedButton = (int)MathHelper.Clamp(SelectedButton - 1, 0, MenuItemText.Count - 1);
            }
        }

        public bool IsPressed(string action, PlayerIndex player, GameTime gameTime)
        {
            gameInput.BeginUpdate();
            if (gameInput.IsPressed(action, player))
            {
                return (true);
            }
            if (gameInput.IsPressed(SelectButtonAction) && action == MenuItemAction[SelectedButton]
                && gameTime.TotalGameTime - previousMenuMoveTime > menuMoveTime)
            {
                previousMenuMoveTime = gameTime.TotalGameTime;
                return (true);
            }

            return (false);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < bgpositions.Length; i++)
            {
                //spriteBatch.Draw(bgtexture, bgpositions[i], Color.White);
            }
            
            spriteBatch.DrawString(font, MenuTitle, 
                new Vector2((ScreenWidth / 2) - 
                (ButtonUp.Width / 2) + 30, 20), 
                Color.White );

            int num = 0;
            foreach (string itemtext in MenuItemText)
            {

                float normalizedLifetime = FadeTimer / FadeForTime;

                float alpha = normalizedLifetime; // *(1 - normalizedLifetime);

                Color color = Color.White * alpha;

                if (SelectedButton == num)
                {
                    spriteBatch.Draw(ButtonDown, MenuItemLocation[num], color);
                    spriteBatch.DrawString(font, itemtext,
                        new Vector2(MenuItemLocation[num].X + 45, MenuItemLocation[num].Y + 20),
                        color);
                }
                else
                {
                    spriteBatch.Draw(ButtonUp, MenuItemLocation[num], color);
                    spriteBatch.DrawString(font, itemtext,
                        new Vector2(MenuItemLocation[num].X + 45, MenuItemLocation[num].Y + 20),
                        color);
                }
                num++;
            }
        }
    }
}
