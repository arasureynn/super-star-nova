﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SuperStarNova
{
    public class Particles
    {
        public Vector2 Acceleration;
        public Vector2 Position;
        public Vector2 Velocity;
        public Color AlphaColor;
        public Texture2D texture;
        public Vector2 particalOrigin;
        private bool active = true;

        private float scale;
        public float Scale
        {
            get { return scale; }
            set { scale = value; }
        }

        private float rotation;
        public float Rotation
        {
            get { return rotation; }
            set { rotation = value; }
        }

        private float rotationSpeed;
        public float RotationSpeed
        {
            get { return rotationSpeed; }
            set { rotationSpeed = value; }
        }

        private float liveForTime;
        public float LiveForTime
        {
            get { return liveForTime; }
            set { liveForTime = value; }
        }

        private float aliveTimer;
        public float AliveTimer
        {
            get { return aliveTimer; }
            set { aliveTimer = value; }
        }

        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public void Initialize(Texture2D texture,
            Vector2 position, 
            Vector2 velocity, 
            Vector2 acceleration,
            float lifetime, 
            float scale, 
            float rotationSpeed,
            Color alphaColor)
        {
            // set the values to the requested values
            this.texture = texture;
            this.Position = position;
            this.Velocity = velocity;
            this.Acceleration = acceleration;
            this.LiveForTime = lifetime;
            this.Scale = scale;
            this.RotationSpeed = rotationSpeed;
            AlphaColor = alphaColor;

            this.AliveTimer = 0.0f;

            particalOrigin.X = this.texture.Width / 2;
            particalOrigin.Y = this.texture.Height / 2;
            
            // set rotation to some random value between 0 and 360 degrees.
            this.Rotation = (float)Managers.ParticleManager.RandomBetween(0, (0 - MathHelper.TwoPi));
        }

        public void Update(GameTime gameTime)
        {
            float gt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            Velocity += Acceleration * gt;
            Position += Velocity * gt;
            Rotation += RotationSpeed * gt;

            AliveTimer += gt;

            if (AliveTimer > LiveForTime)
            {
                active = false;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (Active)
            {
                float normalizedLifetime = AliveTimer / LiveForTime;

                float alpha = 4 * normalizedLifetime * (1 - normalizedLifetime);

                Color color = AlphaColor * alpha;

                float scale = Scale * (.75f + .25f * normalizedLifetime);

                spriteBatch.Draw(texture, Position, new Rectangle(0, 0, texture.Width, texture.Height), color,
                    Rotation, particalOrigin, scale, SpriteEffects.None, 0.0f);
            }
        }
    }
}
