using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SuperStarNova
{
    internal class Sprite
    {
        public int BoundingXPadding;
        public int BoundingYPadding;
        public int CollisionRadius;
        private int currentFrame;
        private int frameHeight;
        private float frameTime = 0.1f;
        private int frameWidth;
        protected List<Rectangle> frames = new List<Rectangle>();
        protected Vector2 location = Vector2.Zero;
        private float rotation;
        public Texture2D texture;
        private float timeForCurrentFrame;
        private Color tintColor = Color.White;
        protected Vector2 velocity = Vector2.Zero;

        public Sprite(
            Vector2 location, Texture2D texture2D,
            Rectangle initialFrame, Vector2 velocity)
        {
            this.location = location;
            texture = texture2D;
            this.velocity = velocity;

            frames.Add(initialFrame);
            frameHeight = initialFrame.Height;
            frameWidth = initialFrame.Width;
        }

        public Vector2 Location
        {
            get { return location; }
            set { location = value; }
        }

        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        public Color TintColor
        {
            get { return tintColor; }
            set { tintColor = value; }
        }

        public float Rotation
        {
            get { return rotation; }
            set { rotation = value; }
        }

        public int Frame
        {
            get { return currentFrame; }
            set { currentFrame = (int) MathHelper.Clamp(value, 0, frames.Count - 1); }
        }
        
        public float FrameTime
        {
            get { return frameTime; }
            set { frameTime = MathHelper.Max(0, value); }
        }

        public Rectangle Source
        {
            get { return frames[currentFrame]; }
        }

        public Rectangle Destination
        {
            get
            {
                return new Rectangle(
                    (int) location.X, (int) location.Y,
                    frameWidth, frameHeight);
            }
        }

        public Vector2 Center
        {
            get
            {
                return location +
                    new Vector2(frameWidth / 2, frameHeight / 2);
            }
        }

        public Rectangle BoundingBoxRect
        {
            get
            {
                return new Rectangle(
                    (int)location.X + BoundingXPadding,
                    (int)location.Y + BoundingYPadding,
                    frameWidth - (BoundingXPadding * 2),
                    frameHeight - (BoundingYPadding * 2));
            }
        }

        public bool IsBoxColliding(Rectangle OtherBox)
        {
            return BoundingBoxRect.Intersects(OtherBox);
        }

        public bool IsCircleColliding(Vector2 otherCenter, float otherRadius)
        {
            if (Vector2.Distance(Center, otherCenter) <
                (CollisionRadius + otherRadius))
                return true;
            else
                return false;
        }

        public void AddFrame(Rectangle frame)
        {
            frames.Add(frame);
        }

        public void Update(GameTime gameTime)
        {
            float elapsed = (float) gameTime.ElapsedGameTime.TotalSeconds;
            timeForCurrentFrame += elapsed;

            if (timeForCurrentFrame >=FrameTime)
            {
                currentFrame = (currentFrame + 1)%(frames.Count);
                timeForCurrentFrame = 0.0f;
            }
            location += (velocity*elapsed);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                texture, Center,
                Source, tintColor,
                rotation, new Vector2(frameWidth/2, frameHeight/2),
                1.0f, SpriteEffects.None, 0.0f);
        }
    }

    internal class Animation
    {
        public bool Active;
        public int FrameHeight;
        public int FrameWidth;
        public bool Looping;
        public Vector2 Position;
        private Color color;
        private int currentFrame;
        private Rectangle destinationRect;
        private int elapsedTime;
        private int frameCount;
        private int frameTime;
        private float scale;
        private Rectangle sourceRect;
        private Texture2D spriteStrip;

        public void Initialize(Texture2D texture, Vector2 position,
                               int frameWidth, int frameHeight, int frameCount,
                               int frameTime, Color color, float scale, bool looping)
        {
            this.color = color;
            FrameHeight = frameHeight;
            FrameWidth = frameWidth;
            this.frameCount = frameCount;
            this.frameTime = frameTime;
            this.scale = scale;

            Looping = looping;
            Position = position;
            spriteStrip = texture;

            elapsedTime = 0;
            currentFrame = 0;
            Active = true;
        }

        public void Update(GameTime gameTime)
        {
            if (Active == false)
                return;

            elapsedTime += (int) gameTime.ElapsedGameTime.TotalMilliseconds;

            if (elapsedTime > frameTime)
            {
                currentFrame++;

                if (currentFrame == frameCount)
                {
                    currentFrame = 0;
                    if (Looping == false)
                        Active = false;
                }
                elapsedTime = 0;
            }

            sourceRect = new Rectangle(currentFrame * FrameWidth,
                0, FrameWidth, FrameHeight);

            destinationRect = new Rectangle((int)Position.X - (int)(FrameWidth * scale) / 2,
                                            (int)Position.Y - (int)(FrameHeight * scale) / 2,
                                            (int)(FrameWidth * scale),
                                            (int)(FrameHeight * scale));
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (Active)
            {
                spriteBatch.Draw(spriteStrip, destinationRect, sourceRect, color);
            }
        }
    }
}