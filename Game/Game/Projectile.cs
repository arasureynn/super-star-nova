﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SuperStarNova
{
    class Projectile
    {
        public Texture2D Texture;
        public Vector2 Position;
        public float Direction;
        public bool Active;
        public int Damage;
        Viewport viewport;

        public int Width
        { get { return Texture.Width; } }
        public int Height
        { get { return Texture.Height; } }

        float projectileMoveSpeed;

        public void Initialize(
            Viewport rect, Texture2D texture, 
            Vector2 position, float direction, 
            int damage, float speed)
        {
            Texture = texture;
            Position = position;
            Direction = MathHelper.ToRadians(direction);
            this.viewport = rect;

            Active = true;
            Damage = damage;
            projectileMoveSpeed = speed;
        }

        public void Update(GameTime time)
        {
            Position.X += (projectileMoveSpeed * (float)Math.Sin(Direction));
            Position.Y += -(projectileMoveSpeed * (float)Math.Cos(Direction));

            if (Position.X + Texture.Width / 2 > viewport.Width || Position.Y + Texture.Height / 2 > viewport.Height)
                Active = false;
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Position, null, Color.White, Direction + MathHelper.ToRadians(90f),
                new Vector2(Width / 2, Height / 2), 1f, SpriteEffects.None, 0f);
        }
    }
}
