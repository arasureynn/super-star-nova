using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SuperStarNova.Managers
{
    class ExplosionManager
    {
        List<Animation> explosions = new List<Animation>();
        Texture2D texture;
        int frameWidth;
        int frameHeight;
        int frameCount;
        int frameTime;
        Color color;
        float scale;
        bool looping;

        public void Initialize(Texture2D texture,
            int frameWidth,
            int frameHeight,
            int frameCount,
            int frameTime,
            Color color,
            float scale,
            bool looping)
        {
            this.texture = texture;
            this.frameWidth = frameWidth;
            this.frameHeight = frameHeight;
            this.frameCount = frameCount;
            this.frameTime = frameTime;
            this.color = color;
            this.scale = scale;
            this.looping = looping;
        }

        public void Add(Vector2 position)
        {
            Animation explosion = new Animation();
            explosion.Initialize(texture, position, frameWidth, frameHeight, frameCount, frameTime, color, scale, looping);
            explosions.Add(explosion);
        }

        public void Add(Vector2 position, float scale)
        {
            Animation explosion = new Animation();
            explosion.Initialize(texture, position, frameWidth, frameHeight, frameCount, frameTime, color, scale, looping);
            explosions.Add(explosion);
        }

        public void Clear()
        {
            explosions.Clear();
        }

        public void Update(GameTime gameTime)
        {
            for (int i = explosions.Count - 1; i >= 0; i--)
            {
                explosions[i].Update(gameTime);
                if (explosions[i].Active == false)
                {
                    explosions.RemoveAt(i);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = explosions.Count - 1; i >= 0; i--)
            {
                explosions[i].Draw(spriteBatch);
            }
        }

    }
}
