using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace SuperStarNova.Managers
{
    class CollisionManager
    {
        private Player playerManager;
        private EnemyManager enemyManager;
        private ParticleManager particleManager;
        private ExplosionManager explosionManager;
          
        private Vector2 offScreen = new Vector2(-500, -500);

        private int MaxX = 0;
        private int MinX = 0;
        private int MaxY = 0;
        private int MinY = 0;

        public CollisionManager(
            Player playerManager,
            EnemyManager enemyManager,
            ParticleManager particleManager,
            ExplosionManager explosionManager,
            GraphicsDevice graphicsDevice)
        {
            this.playerManager = playerManager;
            this.enemyManager = enemyManager;
            this.particleManager = particleManager;
            this.explosionManager = explosionManager;

            MaxX = graphicsDevice.Viewport.TitleSafeArea.Width;
            MinX = graphicsDevice.Viewport.TitleSafeArea.X;
            MaxY = graphicsDevice.Viewport.TitleSafeArea.Height;
            MinY = graphicsDevice.Viewport.TitleSafeArea.Y;
        }

        private void checkShotToEnemyCollisions()
        {
            foreach (var shot in playerManager.PlayerShotManager.Shots)
            {
                foreach (var enemy in enemyManager.Enemies)
                {
                    if (shot.IsCircleColliding(enemy.EnemySprite.Center,
                        enemy.EnemySprite.CollisionRadius))
                    {
                        explosionManager.Add(shot.Location, 0.3f);

                        shot.Location = offScreen;
                        enemy.Health -= playerManager.PlayerShotManager.Damage;
                        particleManager.AddParticles(enemy.EnemySprite.Center, 60);

                        if (enemy.Health <= 0)
                        {
                            particleManager.AddParticles(enemy.EnemySprite.Center,750);
                            if (enemy.IsBoss())
                                particleManager.AddParticles(enemy.EnemySprite.Center);
                            playerManager.Experience += enemy.getXP();
                            playerManager.PlayerScore += enemy.getXP();

                            explosionManager.Add(enemy.EnemySprite.Center);
                        }
                    }
                }
            }
        }

        private void checkShotToPlayerCollision(Rectangle playerRectangle)
        {
            foreach (var shot in enemyManager.EnemyShotManager.Shots)
            {
                if (shot.IsBoxColliding(playerRectangle))
                {
                    explosionManager.Add(shot.Location, 0.3f);
                    particleManager.AddParticles(shot.Location, 60);

                    shot.Location = offScreen;
                    playerManager.Health -= 10;
                }
            }
        }

        private void checkEnemyToPlayerCollision(Rectangle playerRectangle)
        {
            foreach (var enemy in enemyManager.Enemies)
            {
                if (enemy.EnemySprite.IsBoxColliding(playerRectangle))
                {
                    playerManager.Health -= 10;
                    enemy.Health -= 10;

                    particleManager.AddParticles(enemy.EnemySprite.Center);
                }
            }
        }

        private void checkParticleToPlayerCollision(Rectangle playerRectangle)
        {
            foreach (var particle in particleManager.particles)
            {
                Rectangle particleRectangle = new Rectangle((int)particle.Position.X,
                    (int)particle.Position.Y,
                    (int)(particle.texture.Width * particle.Scale),
                    (int)(particle.texture.Height * particle.Scale));

                if (playerRectangle.Intersects(particleRectangle))
                {
                    //particle.Active = false;
                    particle.Acceleration = new Vector2(-1 * particle.Acceleration.X, -1 * particle.Acceleration.Y);
                }
            }
        }

        private void CheckParticleToEdgeCollision()
        {
            foreach (var particle in particleManager.particles)
            {
                if (particle.Position.X > MaxX)
                {
                    particle.Velocity *= 1;
                    particle.Position.X = MaxX;
                }
                if (particle.Position.X < MinX)
                {
                    particle.Velocity *= 1;
                    particle.Position.X = MinX;
                }
                if (particle.Position.Y > MaxY)
                {
                    particle.Velocity *= -1;
                    particle.Position.Y = MaxY;
                }
                if (particle.Position.Y < MinY)
                {
                    particle.Velocity *= -1;
                    particle.Position.Y = MinY;
                }
            }
        }

        public void CheckCollisions()
        {
            Rectangle playerRectangle = new Rectangle
                    ((int)(playerManager.PlayerAnimation.Position.X - (playerManager.PlayerAnimation.FrameWidth / 2)),
                    (int)(playerManager.PlayerAnimation.Position.Y - (playerManager.PlayerAnimation.FrameHeight / 2)),
                    playerManager.PlayerAnimation.FrameWidth,
                    playerManager.PlayerAnimation.FrameHeight);

            checkShotToEnemyCollisions();
            checkEnemyToPlayerCollision(playerRectangle);
            checkParticleToPlayerCollision(playerRectangle);
            checkShotToPlayerCollision(playerRectangle);
            CheckParticleToEdgeCollision();
        }

        public void Update()
        {
            //update explosions
            CheckCollisions();
        }
    }
}
