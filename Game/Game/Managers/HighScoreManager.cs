﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
#if WINDOWS
using Microsoft.Xna.Framework.Storage;
#endif

namespace SuperStarNova.Managers
{
    public struct HighScoreManager
    {
        public string[] PlayerName;
        public int[] Score;
        public int[] Level;

        public int Count;

        public HighScoreManager(int count)
        {
            PlayerName = new string[count];
            Score = new int[count];
            Level = new int[count];

            Count = count;
        }

        public void SaveHighScores(HighScoreManager data,
            string fileName)
        {
#if WINDOWS_PHONE
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            using (var stream =
                new IsolatedStorageFileStream(fileName,
                    FileMode.Create, FileAccess.Write, store))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(HighScoreManager));
                    serializer.Serialize(stream, data);
                }
                finally
                {
                    stream.Close();
                }
            }
#endif
#if WINDOWS
            XmlSerializer serializer = new XmlSerializer(typeof(HighScoreManager));
            Stream streamWriter = new FileStream(fileName, FileMode.Create);
            serializer.Serialize(streamWriter, data);
            streamWriter.Close();
#endif
        }

        public HighScoreManager LoadHighScores(string fileName)
        {
            HighScoreManager data;

#if WINDOWS_PHONE
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            using (var stream =
                new IsolatedStorageFileStream(fileName,
                    FileMode.OpenOrCreate, FileAccess.Read, store))
            using (var reader = new StreamReader(stream))
            {
                try
                {
                    XmlSerializer serializer =
                        new XmlSerializer(typeof(HighScoreManager));
                    data = (HighScoreManager)serializer.Deserialize(stream);
                }
                finally
                {
                    stream.Close();
                }
            }
#endif
#if WINDOWS

            XmlSerializer serializer = new XmlSerializer(typeof(HighScoreManager));
            FileStream fileStream = new FileStream(fileName, FileMode.Open);
            data = (HighScoreManager) serializer.Deserialize(fileStream);
            fileStream.Close();
#endif
            return (data);
        }

        public void Initialize()
        {
            string[] names = { "Chris","Nic","Daniel","Krystal","Sarah","Caleb","Zach","Ralph","Yumi","Marissa" };
            List<int> scores = new List<int>{999,879,841,841,8887,7681,8979,1349,4617,3496};
            scores.Sort();
            
            Random rand = new Random(DateTime.Now.Millisecond);
            System.Diagnostics.Debug.WriteLine(rand.Next(names.Length).ToString());

            HighScoreManager data = new HighScoreManager(10);

            for (int i = 0; i <= data.Count - 1; i++)
            {
                string name = names.ElementAt(rand.Next(names.Length));
                data.PlayerName[i] = name;
                data.Level[i] = rand.Next(1, 9);
                data.Score[i] = scores.IndexOf(i);
            }

            SaveHighScores(data, Constants.HighScoreSaveFile);
        }

        public void SaveHighScores(string playerName, int score, int level)
        {
            var data = LoadHighScores(Constants.HighScoreSaveFile);

            int scoreIndex = -1;
            for (int i = 0; i < data.Count; i++)
            {
                if (score > data.Score[i])
                {
                    scoreIndex = i;
                    break;
                }
            }

            if (scoreIndex > -1)
            {
                //New high score found ... do swaps
                for (int i = data.Count - 1; i > scoreIndex; i--)
                {
                    data.PlayerName[i] = data.PlayerName[i - 1];
                    data.Score[i] = data.Score[i - 1];
                    data.Level[i] = data.Level[i - 1];
                }

                data.PlayerName[scoreIndex] = Constants.DefaultPlayerName; //Retrieve User Name Here
                data.Score[scoreIndex] = score;
                data.Level[scoreIndex] = level;

                SaveHighScores(data, Constants.HighScoreSaveFile);
            }
        }

        public bool CheckForSavedHighScores(string fileName)
        {
#if WINDOWS_PHONE
            return IsolatedStorageFile.GetUserStoreForApplication().FileExists(fileName);
#endif
#if WINDOWS
            return CheckIfExists(fileName);
#endif
        }

        private void DeleteHighScores(string fileName)
        {
#if WINDOWS
            File.Delete(fileName);
#endif
#if WINDOWS_PHONE
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                store.DeleteFile(fileName);
            }
#endif
        }

        public bool CheckIfExists(string fileName)
        {
            return File.Exists(fileName);
        }
    }
}
