﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SuperStarNova.Managers;
using Microsoft.Xna.Framework.Audio;

namespace SuperStarNova
{
    class Player
    {
        public Vector2 Position;
        public bool Active;
        public int Health;
        public int Experience;
        public Animation PlayerAnimation;
        public ShotManager PlayerShotManager;

        public int PlayerScore;

        private Vector2 gunOffset = new Vector2(40, -8);
        private float shotTimer = 0.0f;
        private float minShotTimer = 0.2f;

        private int damage = 10;
        public int Damage
        {
            get { return Damage; }
            set { damage = value; }
        }

        public int Width
        {
            get { return PlayerAnimation.FrameWidth; }
        }

        public int Height
        {
            get { return PlayerAnimation.FrameHeight; }
        }

        public void Initialize(Animation animation, Vector2 position, 
            Texture2D shotTexture,Rectangle screenBounds, SoundEffect lasersound)
        {
            PlayerAnimation = animation;
            Position = position;
            Active = true;
            Health = 100;
            Experience = 0;
            PlayerShotManager = new ShotManager(shotTexture,
                shotTexture.Bounds, 1,2,250f,screenBounds, lasersound);
        }

        public void FireShot(float direction,int damage)
        {
            PlayerShotManager.FireShot(
                Position + gunOffset,
                new Vector2(3, 0),
                damage,
                direction,
                true);
        }

        public bool IsAlive()
        {
            return Active;
        }

        public void Update(GameTime gameTime)
        {
            PlayerAnimation.Position = Position;
            PlayerAnimation.Update(gameTime);
            PlayerShotManager.Update(gameTime);
        }

        public void Draw(SpriteBatch spritez)
        {
            PlayerAnimation.Draw(spritez);
            PlayerShotManager.Draw(spritez);
        }
    }
}
