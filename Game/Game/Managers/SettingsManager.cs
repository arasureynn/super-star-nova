using System.Xml.Serialization;
using System.IO.IsolatedStorage;
using System.IO;

namespace SuperStarNova.Managers
{
    public struct SettingsManager
    {
        public string[] PlayerName;
        public string DefaultPlayerName;

        public SettingsManager(int count)
        {
            PlayerName = new string[count];
            DefaultPlayerName = Constants.DefaultPlayerName;
        }

        public void SaveSettings(SettingsManager data, string fileName)
        {
#if WINDOWS_PHONE
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            using (var stream = new IsolatedStorageFileStream(fileName, FileMode.Create, FileAccess.Write, store))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SettingsManager));
                    serializer.Serialize(stream, data);
                }
                finally
                {
                    stream.Close();
                }
            }
#endif
#if WINDOWS 
            XmlSerializer serializer = new XmlSerializer(typeof(SettingsManager));
            Stream streamWriter = new FileStream(fileName,FileMode.Create);
            serializer.Serialize(streamWriter,data);
            streamWriter.Close();
#endif
        }

        public SettingsManager LoadSettings(string fileName)
        {
            SettingsManager data;
#if WINDOWS_PHONE
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            using (var stream = 
                new IsolatedStorageFileStream(
                    fileName,
                    FileMode.OpenOrCreate, 
                    FileAccess.Read, 
                    store)
                    )
            using (var reader = new StreamReader(stream))
            {
                try
                {
                    XmlSerializer serializer =
                        new XmlSerializer(typeof(SettingsManager));
                    data = (SettingsManager)serializer.Deserialize(stream);
                }
                finally
                {
                    stream.Close();
                }
            }
#endif
#if WINDOWS
            XmlSerializer serializer = new XmlSerializer(typeof(SettingsManager));
            FileStream fileStream = new FileStream(fileName, FileMode.Open);
            data = (SettingsManager)serializer.Deserialize(fileStream);
            fileStream.Close();
#endif
            return (data);
        }

        public void LoadDefaultSettings()
        {
            SettingsManager data = new SettingsManager(1);
            data.PlayerName[0] = DefaultPlayerName;

            SaveSettings(data, Constants.SettingsManagerSaveFile);
        }

        public void SaveSetting(string playerName)
        {

            System.Diagnostics.Debug.WriteLine("Saving name... " + playerName);
            var data = LoadSettings(Constants.SettingsManagerSaveFile);

            data.PlayerName[0] = playerName;

            SaveSettings(data, Constants.SettingsManagerSaveFile);
        }

        public bool CheckForSettingsFile(string fileName)
        {
#if WINDOWS_PHONE
            return IsolatedStorageFile.GetUserStoreForApplication().FileExists(fileName);
#endif
#if WINDOWS
            return CheckIfExists(fileName);
#endif
        }

        private void DeleteSettings(string fileName)
        {
#if WINDOWS
            File.Delete(fileName);
#endif
#if WINDOWS_PHONE
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                store.DeleteFile(fileName);
            }
#endif
        }

        public bool CheckIfExists(string fileName)
        {
            return File.Exists(fileName);
        }
    }
}
