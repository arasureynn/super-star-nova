using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace SuperStarNova.Managers
{
    class EnemyManager
    {
        public ShotManager EnemyShotManager;

        private Levels levels;
        private Texture2D texture;
        
        public List<Enemy2> Enemies = new List<Enemy2>();
        public List<Enemy> EnemyTypes = new List<Enemy>();
        public List<Enemy> BossTypes = new List<Enemy>();

        public float nextWaveTimer = 0.0f;

        private float shipSpawnTimer = 0.0f;
        private float shipSpawnWaitTime = 0.75f;

        private Rectangle screenBounds;

        private List<List<Vector2>> pathWaypoints = new List<List<Vector2>>();
        private List<List<Vector2>> bossPathWaypoints = new List<List<Vector2>>();

        private Dictionary<int, int> waveSpawns = new Dictionary<int, int>();

        public bool Active = true;

        private Random rand = new Random();

        private bool bossMode = false;
        private bool bossActive = false;

        private int type = 0;

        public int enemiesKilledLevel = 0;

        private void setUpEnemyTypes()
        {
            EnemyTypes.Add(new Enemy(
                texture,
                pathWaypoints[0][0],
                new Rectangle(0, 0, 80, 80),
                20,
                10,
                5,
                200,
                10,
                0f,
                false));

            EnemyTypes.Add(new Enemy(
                texture,
                pathWaypoints[0][0],
                new Rectangle(0, 80, 50, 50),
                30,
                20,
                2,
                200,
                30,
                3f,
                false));

            EnemyTypes.Add(new Enemy(
                texture,
                pathWaypoints[0][0],
                new Rectangle(0, 130, 70, 70),
                30,
                20,
                4,
                200,
                40,
                4f,
                false));

            EnemyTypes.Add(new Enemy(
                texture,
                pathWaypoints[0][0],
                new Rectangle(0, 200, 90, 60),
                30,
                30,
                4,
                150,
                50,
                0f,
                false));
        }

        private void setUpBossTypes()
        {
            BossTypes.Add(new Enemy(
                texture,
                pathWaypoints[0][0],
                new Rectangle(0, 261, 67, 100),
                1000,
                20,
                1,
                100,
                1000,
                2f,
                true));
        }

        private void setUpBossWaypoints()
        {
            List<Vector2> path0 = new List<Vector2>();
            path0.Add(new Vector2((int)(screenBounds.Width * .5), (int)(screenBounds.Height * .1)));
            path0.Add(new Vector2((int)(screenBounds.Width * .5), (int)(screenBounds.Height * .7)));
            path0.Add(new Vector2((int)(screenBounds.Width * .8), (int)(screenBounds.Height * .7)));
            path0.Add(new Vector2((int)(screenBounds.Width * .8), (int)(screenBounds.Height * .1)));
            bossPathWaypoints.Add(path0);
        }

        private void setUpWaypoints()
        {
            List<Vector2> path0 = new List<Vector2>();
            path0.Add(new Vector2(screenBounds.Width, (int)(screenBounds.Height * .1)));
            path0.Add(new Vector2((int)(screenBounds.Width * .5), (int)(screenBounds.Height * .1)));
            path0.Add(new Vector2((int)(screenBounds.Width * .25), (int)(screenBounds.Height * .7)));
            path0.Add(new Vector2(-100, (int)(screenBounds.Height * .7)));
            pathWaypoints.Add(path0);
            waveSpawns[0] = 0;

            List<Vector2> path1 = new List<Vector2>();
            path1.Add(new Vector2(screenBounds.Width, (int)(screenBounds.Height * .7)));
            path1.Add(new Vector2((int)(screenBounds.Width * .5), (int)(screenBounds.Height * .7)));
            path1.Add(new Vector2((int)(screenBounds.Width * .25), (int)(screenBounds.Height * .1)));
            path1.Add(new Vector2(-100, (int)(screenBounds.Height * .1)));
            pathWaypoints.Add(path1);
            waveSpawns[1] = 0;

            List<Vector2> path2 = new List<Vector2>();
            path2.Add(new Vector2(screenBounds.Width, (int)(screenBounds.Height * .5)));
            path2.Add(new Vector2((int)(screenBounds.Width * .5), (int)(screenBounds.Height * .5)));
            path2.Add(new Vector2((int)(screenBounds.Width * .5), (int)(screenBounds.Height * .2)));
            path2.Add(new Vector2((int)(screenBounds.Width * .5), (int)(screenBounds.Height * .7)));
            path2.Add(new Vector2(-100, (int)(screenBounds.Height * .7)));
            pathWaypoints.Add(path2);
            waveSpawns[2] = 0;

            List<Vector2> path3 = new List<Vector2>();
            path3.Add(new Vector2(screenBounds.Width, (int)(screenBounds.Height * .5)));
            path3.Add(new Vector2((int)(screenBounds.Width * .5), (int)(screenBounds.Height * .5)));
            path3.Add(new Vector2((int)(screenBounds.Width * .5), (int)(screenBounds.Height * .7)));
            path3.Add(new Vector2((int)(screenBounds.Width * .5), (int)(screenBounds.Height * .2)));
            path3.Add(new Vector2(-100, (int)(screenBounds.Height * .2)));
            pathWaypoints.Add(path3);
            waveSpawns[3] = 0;

            List<Vector2> path4 = new List<Vector2>();
            path4.Add(new Vector2(screenBounds.Width, (int)(screenBounds.Height * .3)));
            path4.Add(new Vector2((int)(screenBounds.Width * .5), (int)(screenBounds.Height * .5)));
            path4.Add(new Vector2((int)(screenBounds.Width * .9), (int)(screenBounds.Height * .5)));
            path4.Add(new Vector2((int)(screenBounds.Width * .9), (int)(screenBounds.Height * .7)));
            path4.Add(new Vector2(-100, (int)(screenBounds.Height * .7)));
            pathWaypoints.Add(path4);
            waveSpawns[4] = 0;

            List<Vector2> path5 = new List<Vector2>();
            path5.Add(new Vector2(screenBounds.Width, (int)(screenBounds.Height * .7)));
            path5.Add(new Vector2((int)(screenBounds.Width * .5), (int)(screenBounds.Height * .5)));
            path5.Add(new Vector2((int)(screenBounds.Width * .9), (int)(screenBounds.Height * .5)));
            path5.Add(new Vector2((int)(screenBounds.Width * .9), (int)(screenBounds.Height * .3)));
            path5.Add(new Vector2(-100, (int)(screenBounds.Height * .3)));
            pathWaypoints.Add(path5);
            waveSpawns[5] = 0;
        }

        public EnemyManager(
            Texture2D texture,
            Rectangle screenBounds,
            Levels levels,
            Texture2D shotTexture,
            SoundEffect lasersound)
        {
            this.texture = texture;
            this.levels = levels;
            this.screenBounds = screenBounds;
            setUpWaypoints();
            setUpEnemyTypes();
            setUpBossWaypoints();
            setUpBossTypes();
            EnemyShotManager = new ShotManager(shotTexture,
                shotTexture.Bounds, 1,2,250f,screenBounds, lasersound);
        }

        private void SpawnBoss(int path, int bosstype)
        {

            var thisBoss = new Enemy2(EnemyTypes[bosstype].getTexture(),
                BossTypes[bosstype].getLocation(),
                BossTypes[bosstype].getInitialFrame(),
                BossTypes[bosstype].getHealth(),
                BossTypes[bosstype].getDamage(),
                BossTypes[bosstype].getFrameCount(),
                BossTypes[bosstype].getSpeed(),
                BossTypes[bosstype].getXP(),
                BossTypes[bosstype].getShotTimer(),
                BossTypes[bosstype].getIsBoss());

            for (int x = 0; x < bossPathWaypoints[path].Count; x++)
            {
                thisBoss.AddWaypoint(bossPathWaypoints[path][x]);
            }
            Enemies.Add(thisBoss);
        }

        public void SpawnEnemy(int path, int enemytype)
        {
            var thisEnemy = new Enemy2(EnemyTypes[enemytype].getTexture(),
                EnemyTypes[enemytype].getLocation(),
                EnemyTypes[enemytype].getInitialFrame(),
                EnemyTypes[enemytype].getHealth(),
                EnemyTypes[enemytype].getDamage(),
                EnemyTypes[enemytype].getFrameCount(),
                EnemyTypes[enemytype].getSpeed(),
                EnemyTypes[enemytype].getXP(),
                EnemyTypes[enemytype].getShotTimer(),
                EnemyTypes[enemytype].getIsBoss());

            for (int x = 0; x < pathWaypoints[path].Count; x++)
            {
                thisEnemy.AddWaypoint(pathWaypoints[path][x]);
            }
            Enemies.Add(thisEnemy);
        }

        private void SpawnWave(int waveType)
        {
            type = levels.GetEnemySpawnType();
            waveSpawns[waveType] +=
                rand.Next(levels.GetMinShipsPerWave(), levels.GetMaxShipsPerWave() + 1);
        }

        private void UpdateWaveSpawns(GameTime gameTime)
        {
            shipSpawnTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (shipSpawnTimer > shipSpawnWaitTime)
            {
                for (int x = waveSpawns.Count - 1; x >= 0; x--)
                {
                    if (waveSpawns[x] > 0)
                    {
                        waveSpawns[x]--;
                        SpawnEnemy(x, type);
                    }
                }
                shipSpawnTimer = 0f;
            }

            nextWaveTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (nextWaveTimer > levels.GetnextWaveMinTimer())
            {
                SpawnWave(rand.Next(0, pathWaypoints.Count));
                nextWaveTimer = 0f;
            }
        }

        public bool BossMode
        {
            get { return bossMode; }
            set { bossMode = value; }
        }
        
        public void FireShot(Vector2 position, float direction, int damage)
        {
            EnemyShotManager.FireShot(position,
                new Vector2(-1 , 0),
                damage,
                direction,
                true);
        }

        public void Update(GameTime gameTime)
        {
            for (int x = Enemies.Count - 1; x >= 0; x--)
            {
                Enemies[x].Update(gameTime);

                if (Enemies[x].fireTime > TimeSpan.FromSeconds(0f) 
                    && Enemies[x].firstWaypointReached == true 
                    && gameTime.TotalGameTime - Enemies[x].previousFireTime > Enemies[x].fireTime)
                {
                    Enemies[x].previousFireTime = gameTime.TotalGameTime;
                    FireShot(Enemies[x].EnemySprite.Center + new Vector2(-20, 0), -2, Enemies[x].Damage);
                }

                if (Enemies[x].IsActive() == false)
                {
                    if(Enemies[x].IsBoss())
                    {
                        bossMode = false;
                        bossActive = false;
                    }

                    Enemies.RemoveAt(x);
                    enemiesKilledLevel++;
                }
            }
            
            if (bossMode && !bossActive)
            {
                SpawnBoss(rand.Next(0, bossPathWaypoints.Count), levels.GetBossType());
                bossActive = true;
            }

            if (Active && !bossMode)
            {
                UpdateWaveSpawns(gameTime);
            }

            EnemyShotManager.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Enemy2 enemy in Enemies)
            {
                enemy.Draw(spriteBatch);
            }

            EnemyShotManager.Draw(spriteBatch);
        }

    }
}
