﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SuperStarNova.Managers
{
    class ParticleManager
    {
        public const int AlphaBlendDrawOrder = 100;
        public const int AdditiveDrawOrder = 200;

        private Texture2D texture;

        public List<Particles> particles = new List<Particles>();

        #region vars
        private int minNumParticles;
        private int maxNumParticles;

        private float minInitialSpeed;
        private float maxInitialSpeed;

        private float minAcceleration;
        private float maxAcceleration;

        private float minRotationSpeed;
        private float maxRotationSpeed;

        private float minLifetime;
        private float maxLifetime;

        private float minScale;
        private float maxScale;

        public List<Color> particleColor = new List<Color>();

        private static Random random = new Random();

        #endregion

        public ParticleManager(Texture2D texture,
            int minNumParticles,
            int maxNumParticles, 
            float minLifetime, 
            float maxLifetime, 
            float minScale, 
            float maxScale,
            float minInitialSpeed,
            float maxInitialSpeed,
            float minAcceleration,
            float maxAcceleration,
            float minRotationSpeed,
            float maxRotationSpeed)
        {
            this.texture = texture;


            this.minNumParticles = minNumParticles;
            this.maxNumParticles = maxNumParticles;

            this.minAcceleration = minAcceleration;
            this.maxAcceleration = maxAcceleration;

            this.minLifetime = minLifetime;
            this.maxLifetime = maxLifetime;

            this.minScale = minScale;
            this.maxScale = maxScale;

            this.minInitialSpeed = minInitialSpeed;
            this.maxInitialSpeed = maxInitialSpeed;

            this.minRotationSpeed = minRotationSpeed;
            this.maxRotationSpeed = maxRotationSpeed;

            particleColor.Add(Color.White);
        }

        public static Random Random
        {
            get { return random; }
        }

        public static float RandomBetween(float min, float max)
        {
            return min + (float)random.NextDouble() * (max - min);
        }

        public void AddParticles(Vector2 where)
        {
            int numParticles = (int)MathHelper.Clamp(Random.Next(0, maxNumParticles), 0, (maxNumParticles - particles.Count));
            Color alphaColor = particleColor[(int)RandomBetween(0, particleColor.Count)];

            for (int i = 0; i < numParticles; i++)
            {
                InitializeParticle(new Particles(), where, alphaColor); 
            }
        }

        public void AddParticles(Vector2 where, int maxParticles)
        {
            int numParticles = (int)MathHelper.Clamp(
                Random.Next(0, maxNumParticles), 0,
                MathHelper.Clamp((maxNumParticles - particles.Count), 0, maxParticles));
            Color alphaColor = particleColor[(int)RandomBetween(0, particleColor.Count)];

            for (int i = 0; i < numParticles; i++)
            {
                InitializeParticle(new Particles(), where, alphaColor);
            }
        }

        public void AddAlphaColor(Color AlphaColor)
        {
            particleColor.Add(AlphaColor);
        }

        private void InitializeParticle(Particles p, Vector2 where, Color alphaColor)
        {
            Vector2 direction = PickRandomDirection();

            float velocity = RandomBetween(minInitialSpeed, maxInitialSpeed);
            float acceleration = RandomBetween(minAcceleration, maxAcceleration);
            float lifetime = RandomBetween(minLifetime, maxLifetime);
            float scale = RandomBetween(minScale, maxScale);
            float rotationSpeed = RandomBetween(minRotationSpeed, maxRotationSpeed);
            

            p.Initialize(texture, where, velocity * direction, acceleration * direction,
                lifetime, scale, rotationSpeed, alphaColor);
            particles.Add(p);
        }

        private Vector2 PickRandomDirection()
        {
            float angle = RandomBetween(0, MathHelper.TwoPi);
            return new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
        }

        public void Update(GameTime gameTime)
        {
            for (int x = particles.Count - 1; x >= 0; x--)
            {
                particles[x].Update(gameTime);

                if (!particles[x].Active)
                {
                    particles.RemoveAt(x);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Particles p in particles)
            {
                p.Draw(spriteBatch);
            }
        }
    }
}
